# Template plugin for Spigot/Bukkit for a quick start.

## Features
- Command
- Logger
- Plugin

## How to use
1. Clone repo & delete .git directory
2. Change main class name
3. Change package name (for dubcat plugins package names follow this convention `cz.dubcat.yourplugin`)
3. Change `plugin.yml` (main class path, author, version)