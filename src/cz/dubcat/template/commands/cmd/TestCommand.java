package cz.dubcat.template.commands.cmd;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import cz.dubcat.template.commands.CommandInterface;

public class TestCommand implements CommandInterface {

    @Override
    public void onCommand(CommandSender paramCommandSender, Command paramCommand, String paramString, String[] paramArrayOfString) {
        paramCommandSender.sendMessage("Command test executed");
    }

}
