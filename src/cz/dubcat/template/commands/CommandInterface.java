package cz.dubcat.template.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public interface CommandInterface {
	public void onCommand(CommandSender paramCommandSender, Command paramCommand, String paramString, String[] paramArrayOfString);
}
