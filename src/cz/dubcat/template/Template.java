package cz.dubcat.template;

import java.util.Random;
import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

import cz.dubcat.template.commands.CommandHandler;
import cz.dubcat.template.commands.cmd.TemplateCommand;
import cz.dubcat.template.commands.cmd.TestCommand;


public class Template extends JavaPlugin{

    public static Random r = new Random();
    private static Logger log;
    private static Template plugin;
    
    @Override
    public void onEnable() {
        plugin = this;
        log = getLog();
        
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        registerCommands();
        
        //regiter events etc.
        
        log.info("Enabled.");
    }
    
    @Override
    public void onDisable() {
        log.info("Disabled.");
    }
    
    private void registerCommands() {
        CommandHandler handler = new CommandHandler("template");
        //register menu/info handler (for command itself in this case /template)
        handler.registerMenu(new TemplateCommand());
        
        //Sub commands
        //test eg. /template test
        handler.register("test", new TestCommand()); 
        //handler.register("teleport", new TeleportCommand()); 
        
        //register command in bukkit and assign our handler
        getCommand("template").setExecutor(handler);
    }
    
    
    public Template getPlugin() {
        return plugin;
    }
    
    public static Logger getLog() {
        return log;
    }
}
